#include "bmp.h"
#include "rotate.h"
#include <assert.h>

#define ARGS 4
#define INPUT_FN 1
#define OUTPUT_FN 2
#define ANGLE 3

#define READ "rb"
#define WRITE "wb"
