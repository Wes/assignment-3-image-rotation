#include "main.h"
#include <stdlib.h>

int main( int argc, char** argv ) {
    assert(argc >= ARGS && "Format: ./image-transformer <source-image> <transformed-image> <angle>");
    
    FILE *in  = fopen(argv[INPUT_FN], READ);
    FILE *out = fopen(argv[OUTPUT_FN], WRITE);
    assert(in != NULL && out != NULL && "Error in open input/output file");
    int angle = atoi(argv[ANGLE]);
    
    struct image sourced;
    
    enum read_status readed = from_bmp(in, &sourced);
    fclose(in);
    assert(readed == READ_OK && "Error in reading input file");
    
    
    struct image* resulted = NULL;
    if (angle == 0) 
        resulted = &sourced;
    else if (angle == 90 || angle == -270)
        resulted = rotate_90(sourced);
    else if (angle == 180 || angle == -180)
        resulted = rotate_180(sourced);
    else if (angle == 270 || angle == -90)
        resulted = rotate_270(sourced);
    else
        assert(0 && "WRONG ANGLE");
    
    enum write_status writed = to_bmp(out, resulted);
    fclose(out);
    assert(writed == WRITE_OK && "Error WRITING Image");
    free(resulted->data);
    if (angle != 0)
        free(resulted);
    return 0;
}
