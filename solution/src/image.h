#ifndef IMAGE_H
#define IMAGE_H

#include <inttypes.h>
#include <malloc.h>
#include <stddef.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint32_t width, height;
    struct pixel* data;
};

struct image* create_image(uint32_t width, uint32_t height);
#endif
