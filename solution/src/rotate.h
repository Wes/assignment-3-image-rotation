#ifndef ROTATE_H
#define ROTATE_H

#include "image.h"

struct image* rotate_90( struct image const source );
struct image* rotate_180( struct image const source );
struct image* rotate_270( struct image const source );
#endif
