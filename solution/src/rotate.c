#include "rotate.h"


struct image* rotate_90(struct image const source) {
    struct image* rot = create_image(source.height, source.width);
    if (rot == NULL)
        return NULL;

    for (uint32_t x = 0; x < source.width; ++x)
        for (uint32_t y = 0; y < source.height; ++y)
            rot->data[y + (source.width - x - 1) * source.height] = source.data[x + y * source.width];
    
    free(source.data);
    return rot;
}

struct image* rotate_180(struct image const source) {
    struct image* rot = create_image(source.width, source.height);
    if (rot == NULL)
        return NULL;
    
    for (uint32_t x = 0; x < source.width; ++x)
        for (uint32_t y = 0; y < source.height; ++y)
            rot->data[(source.width - x - 1) + (source.height - y - 1) * source.width] = source.data[x + y * source.width];

    
    free(source.data);
    
    return rot;
}

struct image* rotate_270(struct image const source) {
    struct image* rot = create_image(source.height, source.width);
    if (rot == NULL)
        return NULL;
    for (uint32_t x = 0; x < source.width; ++x)
        for (uint32_t y = 0; y < source.height; ++y)
            rot->data[(source.height - y - 1) + x * source.height] = source.data[x + y * source.width];
    
    free(source.data);

    return rot;
}
