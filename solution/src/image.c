#include "image.h"


struct image* create_image(uint32_t width, uint32_t height) {
    struct image *created = malloc(sizeof(struct image));
    if (created == NULL)
        return NULL;

    created->height = height;
    created->width = width;
    struct pixel* datas = malloc(width * height * sizeof(struct pixel));
    if (datas == NULL) {
        free (created);
        return NULL;
    }
    created->data = datas;
    return created;
}
