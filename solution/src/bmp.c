#include "bmp.h"

// Constants
const uint16_t IMAGE_SIGNATURE = 0x4D42;
const uint8_t BYTES_PER_PIXEL = 3;
const uint8_t BYTE_PADD = 4;


enum read_status from_bmp(FILE *input, struct image *image) {
    struct bmp_header head;
    fread(&head, sizeof(struct bmp_header), 1, input);

    if (head.bfType != IMAGE_SIGNATURE)
        return READ_INVALID_SIGNATURE;
    
    uint32_t width = image->width = head.biWidth;
    uint32_t height = image->height = head.biHeight;
    struct pixel* datas = malloc(width * height * sizeof(struct pixel));
    if (datas == NULL)
        return READ_MEMORY_ERROR;
    image->data = datas;

    fseek(input, head.bOffBits, SEEK_SET);

    uint8_t padding = (BYTE_PADD - ((width * BYTES_PER_PIXEL) % BYTE_PADD)) % BYTE_PADD;

    for (uint32_t y = 0; y < height; ++y) {
        for (uint32_t x = 0; x < width; ++x)
            fread(&(image->data[x + y * width]), sizeof(struct pixel), 1, input);
        fseek(input, padding, SEEK_CUR);
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *output, struct image *image) {
    struct bmp_header head = {
        .bfType = IMAGE_SIGNATURE,
        .bfileSize = sizeof(struct bmp_header) + image->width * image->height * sizeof(struct pixel),
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = 40,
        .biWidth = image->width,
        .biHeight = image->height,
        .biPlanes = 1,
        .biBitCount = BYTES_PER_PIXEL * 8,
        .biCompression = 0,
        .biSizeImage = image->width * image->height * sizeof(struct pixel),
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };

    fwrite(&head, sizeof(struct bmp_header), 1, output);

    uint8_t padding = (BYTE_PADD - ((image->width * BYTES_PER_PIXEL) % BYTE_PADD)) % BYTE_PADD;

    const uint8_t zeroes[4] = {0};

    for (uint32_t y = 0; y < image->height; ++y) {
        for (uint32_t x = 0; x < image->width; ++x) {
            if (fwrite(&(image->data[x + y * image->width]), sizeof(struct pixel), 1, output) != 1)
                return WRITE_ERROR;
        }
        fwrite(zeroes, (uint64_t)padding, 1, output);    
    }
    return WRITE_OK;

}
